# **Introduction**
------
The automatic DNS Server Installation Script.

# **Usage**
------
Please modify the dns-db, dns-zone and install file. Modify them to your domain name and your host name to IP mapping.
```
$ ./install
```

# **Important**
------
Remember to open the DNS port 53 for TCP and UDP in IPv4 and IPv6 firewall (iptables).

# **Author**

Jason Chen
